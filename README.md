# Fresher Angualar
Setup Angular:
Đầu tiên các bạn vào trang https://nodejs.org/en/download/ để tải về Nodejs và cài đặt vào máy. Các bạn có thể tải về bản Long Term Support (LTS) hoặc Current đều được. Hiện tại bản LTS mới nhất là version 12, với Angular version 9 thì đã hoàn toàn phù hợp rồi. Đối với các bạn nào quen thuộc với terminal thì mình khuyến cáo sử dụng NVM để cài đặt và quản lý nhiều phiên bản Nodejs trên cùng 1 máy. Như thế các bạn sẽ linh động khi làm việc với nhiều dự án khác nhau có đòi hỏi khác nhau về version của Nodejs. Sau khi cài đặt thành công các bạn có thể verify lại bằng cách mở Terminal/PowerShell/CMD (mình sẽ gọi chung là Terminal) và gõ các lệnh sau, nếu nó hiển thị ra được version thì đã cài đặt thành công.

node -v

npm -v 

Sử dụng công cụ chính thực của Angular: angular-cli thông qua NPM bằng câu lệnh:

npm install -g @angular/cli

Có thể tạo project mới bằng cách mở terminal và thực hiện câu lệnh: 

ng new <tên-project>

hoặc clone project start của primeNg theo câu lệnh (project dự án hầu hết sử dụng các module của primeNg): 

git clone https://github.com/primefaces/primeng-quickstart-cli.git


Sau khi thực hiện xong yêu cầu tuần 1:

Cài đặt api, database: mariaDB như sau: 

B1: bật terminal, query: cd api

B2: Cài đặt thự viện, query: npm install

B3: download MariaDb tại: https://mariadb.org/download/

Khi cài đặt thì đặt password: 123123

B4: truy cập vào hedisql query: 

CREATE DATABASE intern CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE intern_angualar_data(

   id INT NOT NULL AUTO_INCREMENT,

   code VARCHAR(100) NOT NULL,

   name VARCHAR(400) NOT NULL,

   description VARCHAR(40) NULL,

   status int NOT NULL,

   PRIMARY KEY ( id )

);

INSERT INTO intern_angualar_data (code, name, description, status)

VALUES ('Table', 'Bảng', 'Mô tả bảng', 1),

('Column', 'Cột', 'Mô tả cột', 1),

('Row', 'Dòng', 'Mô tả dòng', 1),

('False', 'Dữ liệu sai', 'Thực hiện xoá dòng này', 1),

('True', 'Dữ liệu sai', 'Cập nhật tên dòng này thành: dữ liệu đúng ', 1);


Call API

// Check xem api có đang hoạt động không

GET http://localhost:3000 

Sử dụng get insert update delete 

POST http://localhost:3000/intern-api/get
