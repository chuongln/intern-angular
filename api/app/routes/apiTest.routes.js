module.exports = app => {
  const api_test = require('../controllers/dm/intern_api.controller');
  
  //#region DM nghề nghiệp
	// Retrieve data with condition in body
  app.post('/intern-api/get', api_test.get);

  // Create a new item
  app.post('/intern-api/insert', api_test.create);
  
  // Update a item with id
	app.post('/intern-api/update', api_test.update);

	// Delete a item with id
	app.post('/intern-api/delete', api_test.delete);
  //#endregion
}