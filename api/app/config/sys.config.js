module.exports = {
  CONG_TY: "F.I.T Group",
  TRUNG_TAM: "Công ty cổ phần Tập đoàn F.I.T (FIT)",
  EXCEL_TEMPLATE: "templates/excel/",
  WORD_TEMPLATE: "templates/word/",

  // REDIS
  REDIS_INFO: "userInfo",
  REDIS_MENU: "menu",
  REDIS_PERMISSION: "permission",
  REDIS_ANHCANBO: "anhCanBo",
  REDIS_PORTAL_INFO: "portalUserInfo",
  REDIS_PORTAL_PERMISSION: "portalPermission",

  // INTEGRATION
  BiHRP: "https://fit.hrmonline.vn/",
};
